#!/bin/bash

FILE=/tmp/tor-exits

curl -s -o ${FILE} https://check.torproject.org/exit-addresses
if [ $? == 0 ]; then
    cat <<'EOF' > /tmp/nginx-tor-ip.conf
geo $tor_user {
    default 0;
EOF

    grep ExitAddress ${FILE} | awk '{ print $2 }' | sort | uniq | sed "s/$/ 1;/g" >> /tmp/nginx-tor-ip.conf
    echo "}" >> /tmp/nginx-tor-ip.conf
    mount --bind /tmp/nginx-tor-ip.conf /etc/nginx/conf.d/tor-ip.conf
	if [ nginx -t ]; then
        umount /etc/nginx/conf.d/tor-ip.conf
        mv /tmp/nginx-tor-ip.conf /etc/nginx/conf.d/tor-ip.conf
		rm -f ${FILE}
		service nginx reload
    else umount /etc/nginx/conf.d/tor-ip.conf
	fi
fi

